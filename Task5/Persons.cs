﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task5
{
    class Persons
    {
        private List<Person> personList;

        /// <summary>
        /// Constructor for no arguments:
        /// </summary>
        public Persons()
        {
            PersonList = new List<Person>();
        }
        /// <summary>
        /// Constructor for one argument:
        /// </summary>
        /// <param name="newPerson">Use person class</param>
        public Persons(Person newPerson)
        {
            PersonList = new List<Person>() { newPerson };
        }
        /// <summary>
        /// Constructor for more than one argument:
        /// </summary>
        /// <param name="newPersons"></param>
        public Persons(params Person[] newPersons)
        {
            PersonList = new List<Person>();
            foreach (Person newPerson in newPersons)
            {
                PersonList.Add(newPerson);
            }
            
        }

        public List<Person> PersonList { get => personList; set => personList = value; }

        /// <summary>
        /// Function for letting user search after, and display partial name matches 
        /// in Console.
        /// </summary>
        /// <returns>true if partial match exists</returns>
        public bool partialSearch()
        {
            string cont;
            bool foundPerson = false;
            do
            {
                Console.WriteLine("Search after name!");
                string nameSearch = Console.ReadLine();

                foreach(Person person in PersonList)
                {
                    if (person.ToString().ToLower().Contains(nameSearch.ToLower()))
                    {
                        Console.WriteLine(person.ToString());
                        foundPerson = true;
                    }
                }

                Console.WriteLine("Do you want to search again? y/n");

                do
                {
                    /*Let user continue searching*/
                    cont = Console.ReadLine().Trim();
                    if (!(cont == "y" || cont == "n"))
                    {
                        Console.WriteLine("Please enter 'y' or 'n'");
                    }
                } while (!(cont == "y" || cont == "n"));


            } while (cont == "y");
            return foundPerson;
        }
    }
}
