﻿using System;
using System.Collections.Generic;

namespace Task5
{
    class Program
    {
        static void Main(string[] args)
        {
            Persons collectionOfPersons = new Persons(new Person("John", "Tomson"), new Person("Ole", "Garder"),
                new Person ("Tom", "Skarsgard"), new Person("Donald", "Trump"), new Person("Trond", "Jo"));

            if (!collectionOfPersons.partialSearch())
            {
                Console.WriteLine("Found no match");
            }      
        }
    }
}
