﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task5
{
    public class Person
    {
        // Constructor that takes no arguments:
        public Person()
        {
            FirstName = "unknown";
            Surname = "unknown";
        }

        // Constructor that takes one argument:
        public Person(string name)
        {
            FirstName = name;
            Surname = "unknown";
        }
        public Person(string name, string surname)
        {
            FirstName = name;
            Surname = surname;
        }
        private string phoneNr;

        // Auto-implemented readonly property:
        public string FirstName { get; set; }

        public string Surname { get; set; }
        public string PhoneNr { get => phoneNr; set => phoneNr = value; }

        // Method that overrides the base class (System.Object) implementation.
        public override string ToString()
        {
            return $"{FirstName} {Surname}";
        }
    }
}
